<?php
namespace App\hobbies;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class hobbies extends DB{
    public $id;
    public $name="user";
    public $hobbies="";

    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data=NULL){

        if(array_key_exists('id',$data))          //if (if(array_key_exists('id',$data) use kora hoi
        {
            $this->id =$data['id'];
        }
        if(array_key_exists('name',$data)){


            $this->name= $data['name'];

        }
        if(array_key_exists('hobbies',$data)){

            $this->hobbies = $data['hobbies'];

        }

    }



        public function store(){
            $str=implode($this->hobbies);
            $arrData = array($this->name,$this->hobbies);
            $sql = "INSERT INTO hobbies(name,hobbies) VALUES (?,?)";

            $STH = $this->DBH->prepare($sql);

            $result = $STH->execute($arrData);
            if($result)
                Message::message("Data has been inserted successfully! :)");
            else
                Message::message("Your Data does not inserted. :(");

            Utility::redirect('create.php');

        }

    // end of store method



    public function index($fetchMode='ASSOC'){
      $sql = "SELECT * from book_title where is_deleted = 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();




    public function view($fetchMode='ASSOC'){
        $sql= 'SELECT * from hobbies where id='.$this->id;
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();


    public function update(){

        $arrData = array ($this->book_title, $this->author_name);
        $sql = "UPDATE book_title SET book_title = ?, author_name = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('index.php');

    }// end of update()


    public function delete(){

        $sql = "Delete from hobbies where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of delete()


    public function trash(){

        $sql = "Update book_title SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()



    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from book_title where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();





    public function recover(){

        $sql = "Update book_title SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of recover();



}// end of BookTitle Class

?>

